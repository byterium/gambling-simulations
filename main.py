import matplotlib.pyplot as plt
from random import choices

STARTING_BALANCE = 1000
SIMULATION_COUNT = 10
MAX_ROUNDS = 100000
BET = 10
HOUSE_ADVANTAGE_PCT = 0

for i in range(0, SIMULATION_COUNT):
    rounds = []
    wins_and_losses = choices([BET, -BET], [1, 1 + (HOUSE_ADVANTAGE_PCT / 100)], k=MAX_ROUNDS)
    for change in wins_and_losses:
        if len(rounds) == 0:
            rounds.append(STARTING_BALANCE)
        current_balance = rounds[-1]
        new_balance = max(current_balance + change, 0)
        rounds.append(new_balance)
        if new_balance == 0:
            break
    plt.plot(rounds)

plt.xlabel('round')
plt.ylabel('balance')

plt.show()